# README #

### Front End Basic Course Topics ###

* HTML5
* CSS3
* GIT
* PS
* LESS
* JS
* NPM

### How to run it ###

* run npm install
* to use LESS 
- npm install -g less
- npm install -g less-watch-compiler
* to run less
- less-watch-compiler

### Thanks to ###

* node developers
* jonycheung for dead simple less compiler https://github.com/jonycheung/deadsimple-less-watch-compiler/
* GraphicBurger for the design http://graphicburger.com/
* codicamp team
* all participants